from cross import *
from tucker import *
from cross_conv import newton_galerkin, cross_conv, lr_circulant
from cross_multifun import cross_multifun