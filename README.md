Description
===========

This repo is for numerical experiments which are presented in the article "Fast multidimensional convolution in low-rank formats via cross approximation"

Installation
============

First, to clone this repository run
```
git clone git@bitbucket.org:rakhuba/crossconv-experiment.git
```
Then go to 'maxvol' directory:
```
cd tucker3d/core/maxvol
```
and run
```
python setup.py build_ext --inplace
```
